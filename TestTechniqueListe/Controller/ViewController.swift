//
//  ViewController.swift
//  TestTechniqueListe
//
//  Created by savemywater on 29/05/2018.
//  Copyright © 2018 test. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import RealmSwift
import RxSwift
import RxCocoa
import UIKit

class ViewController: UIViewController {

    var list: Variable<[User]> =  Variable([])
    let disposeBag = DisposeBag()
    @IBOutlet weak var collectionView: UICollectionView!
    var realm : Realm!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // On recupere la BDD
        realm = try! Realm()
        
        // Utilisation de Rx pour binding grille
        list.asObservable()
            .bind(to: self.collectionView!.rx.items(cellIdentifier: "customCell", cellType: CustomViewCell.self)) { (row, user, cell) in
                cell.display(user: user)
            }
            .disposed(by: disposeBag)
        
        // Telechargement des donnees
        self.download()

        // Ajout refresh grille
        self.collectionView!.refreshControl = self.refreshControl
    }
    
    // Fonction de recuperation des donnees
    func download() {
        // Si connexion telecharge sinon recupere depuis BDD
        if Reachability.isConnectedToNetwork() {
            let URL = "https://api.github.com/repos/apple/swift/contributors"
            Alamofire.request(URL).responseArray { (response: DataResponse<[User]>) in
                self.list.value = response.result.value!
                try! self.realm.write {
                    self.realm.add(response.result.value!, update: true)
                }
                self.refreshControl.endRefreshing()
            }
        } else {
            list.value = Array(realm.objects(User.self))
            self.refreshControl.endRefreshing()
        }
    }
    
    // Gestion refresh grille
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.download()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

// Gestion clique sur un item de la grille
extension ViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let user = list.value[indexPath.row]
        
        if Reachability.isConnectedToNetwork() {
            let URL = "https://api.github.com/users/" + user.login
            Alamofire.request(URL).responseObject { (response: DataResponse<Details>) in
                try! self.realm.write {
                    user.details = response.value!
                }
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let destination = storyboard.instantiateViewController(withIdentifier: "detailsViewController") as! DetailsViewController
                destination.user = user
                self.navigationController?.pushViewController(destination, animated: true)
            }
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let destination = storyboard.instantiateViewController(withIdentifier: "detailsViewController") as! DetailsViewController
            destination.user = user
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
}
