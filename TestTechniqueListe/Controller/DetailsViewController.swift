//
//  DetailsViewController.swift
//  TestTechniqueListe
//
//  Created by savemywater on 29/05/2018.
//  Copyright © 2018 test. All rights reserved.
//

import Kingfisher
import UIKit

class DetailsViewController: UIViewController {

    var user: User = User()
    
    // IBOutlet pour les differents affichages
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var hidden: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if user.url_picture != "" {
            picture.kf.setImage(with: URL(string: user.url_picture))
            picture.contentMode = .scaleAspectFill
            picture.layer.cornerRadius = picture.frame.size.width / 2
            picture.clipsToBounds = true
        } else {
            picture.image = #imageLiteral(resourceName: "image_placeholder")
            picture.contentMode = .scaleToFill
        }
        
        login.text = user.login
        
        // Si contenu non disponible on cache et affiche un message
        if user.details != nil {
            name.text = user.details!.name
            followers.text = "Nombre followers : " + String(user.details!.nb_followers)
            if user.details!.company != nil {
                company.text = "Société : " + user.details!.company!
            } else {
                company.text = "Société : Inconnue"
            }
            if user.details!.location != nil {
                location.text = "Localisation : " + user.details!.location!
            } else {
                location.text = "Localisation : Inconnue"
            }
        } else {
            name.isHidden = true
            company.isHidden = true
            location.isHidden = true
            followers.isHidden = true
            hidden.isHidden = false
        }
    }
    
    // Gestion clique sur bouton
    @IBAction func seeMore() {
        UIApplication.shared.open(URL(string : user.url_profile)!, options: [:], completionHandler: { (status) in
            })
    }
}
