//
//  Details.swift
//  TestTechniqueListe
//
//  Created by savemywater on 29/05/2018.
//  Copyright © 2018 test. All rights reserved.
//

import ObjectMapper
import RealmSwift

@objcMembers class Details: Object, Mappable {
    
    // Variables
    dynamic var name: String = ""
    dynamic var company: String? = ""
    dynamic var location: String? = ""
    dynamic var nb_followers: Int = 0
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    // Mapping du JSON
    func mapping(map: Map) {
        name <- map["name"]
        company <- map["company"]
        location <- map["location"]
        nb_followers <- map["followers"]
    }
    
}
