//
//  CustomViewCell.swift
//  TestTechniqueListe
//
//  Created by savemywater on 29/05/2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import Kingfisher

// Cellule custom pour la grille
class CustomViewCell : UICollectionViewCell{
    
    // Variables
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var picture: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // Gestion de l'affichage
    func display(user:User) {
        loginLabel.text = user.login
        if user.url_picture != "" {
            picture.kf.setImage(with: URL(string: user.url_picture))
            picture.contentMode = .scaleAspectFill
            picture.layer.cornerRadius = picture.frame.size.width / 2
        } else {
            picture.image = #imageLiteral(resourceName: "image_placeholder")
            picture.contentMode = .scaleToFill
        }
        picture.clipsToBounds = true
    }
    
}
