//
//  User.swift
//  TestTechniqueListe
//
//  Created by savemywater on 29/05/2018.
//  Copyright © 2018 test. All rights reserved.
//

import ObjectMapper
import RealmSwift

@objcMembers class User: Object, Mappable {
    
    // Variables
    dynamic var id: CLongLong = -1
    dynamic var login: String = ""
    dynamic var url_picture: String = ""
    dynamic var url_profile: String = ""
    dynamic var details: Details?
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    // Mapping du JSON
    func mapping(map: Map) {
        id <- map["id"]
        login <- map["login"]
        url_picture <- map["avatar_url"]
        url_profile <- map["html_url"]
    }
    
    // Gestion primary Key
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
